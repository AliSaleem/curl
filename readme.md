# cURL #
**An easy to use OOP wrapper for cURL**

### Set Up ###
- Add package to your project using composer

  `composer require alisalemm/curl:dev-master`
  
- Include autoloader in your PHP file

  `require 'vendor/autoload.php';`
  
- Select correct namespace for the classes in your PHP file
```php
use \AliSaleem\Curl\Request;
use \AliSaleem\Curl\Response;
```

### Examples ###

#### GET Request ####
```php
$request = new Request('https://requestb.in/xxxxxxxx');
$response = $request->get();

echo $response->getBody();
```

#### GET Request with Parameters ####
```php
$response = $request
    ->setParameters([
        'paramA' => 'valueA',    
        'paramB' => 'valueB',    
    ])
    ->addParameter('paramC', 'valueC')
    ->get();
```

#### POST Request ####
```php
$response = $request
    ->setBody('body content')
    ->post();
```
The **body content** can be of the following types

* String
* Array
* Any object that implements `JsonSerializable`

#### Other Request Methods ####
```php
$response = $request->put();
$response = $request->delete();
$response = $request->options();
$response = $request->head();
```

#### Custom Request Method ####
```php
$response = $request->request('CUSTOM_METHOD');
```

#### Add Headers ####
```php
$response = $request
    ->setHeaders([
        'headerA' => 'valueA',    
        'headerB' => 'valueB',    
    ])
    ->addHeader('headerC', 'valueC')
    ->get();
```

#### Customising Wrapper Options ####
```php
$response = $request
    ->setOptions([
        'optionA' => 'valueA',    
        'optionB' => 'valueB',    
    ])
    ->setOption('optionC', 'valueC')
    ->get();
```
The following options can be set

Option | Description | Default Value
------ | ----------- | -------------
`return_body` | `CURLOPT_RETURNTRANSFER` | `true`
`follow_redirects` | `CURLOPT_FOLLOWLOCATION` | `true`
`header` | `CURLOPT_HEADER` | `true`
`return_header` | `CURLINFO_HEADER_OUT` | `true`
`timeout` | `CURLOPT_TIMEOUT` | `30`
`verify_peer` | `CURLOPT_SSL_VERIFYPEER` | `false`
`delay` | delay (seconds) between retry attempts | `0`
`tries` | max request attempts if validation fails | `1`
`multiple_headers` | allow multiple headers | `false`

#### Adding a validation method for responses ####
Retries are attempted if the validation fails
```php
use \AliSaleem\Curl\Request;
use \AliSaleem\Curl\Response;

class MyClass {

    public function myValidationMethod(Response $response)
    {
        // $response->getHttpCode();
        // $response->getHeaders();
        // $response->getHeader('headerName');
        // $response->getBody();
        // $response->getInfo();
        // $response->getError();
        // $response->getErrorNo();
        
        // return true | false;
    }
    
    public function makeCurlRequest()
    {
        $request = new Request('https://requestb.in/xxxxxxxx');

        $response = $request
            ->setOption('tries', 3)
            ->setValidationMethod($this, 'myValidationMethod')
            ->get();
            
        echo $response->getBody();
    }
}
```

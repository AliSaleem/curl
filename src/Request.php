<?php

namespace AliSaleem\Curl;

class Request
{
    protected $method;
    protected $url;
    protected $parameters;
    protected $body;
    protected $headers;
    protected $options;
    protected $validateFunction;
    protected $curlHandle;
    protected $httpAuth;
    protected $responseModel;

    public function __construct(
        $url,
        $parameters = [],
        $body = null,
        $headers = [],
        $options = [],
        $function = []
    ) {
        $this
            ->setUrl($url)
            ->setParameters($parameters)
            ->setBody($body)
            ->setHeaders($headers)
            ->setOptions($options)
            ->setResponseModel(Response::class);
        if (isset($function[0]) && isset($function[1])) {
            $this->setValidateFunction($function[0], $function[1]);
        }
    }

    public function get()
    {
        return $this->request('GET');
    }

    public function put()
    {
        return $this->request('PUT');
    }

    public function post()
    {
        return $this->request('POST');
    }

    public function delete()
    {
        return $this->request('DELETE');
    }

    public function options()
    {
        return $this->request('OPTIONS');
    }

    public function head()
    {
        return $this->request('HEAD');
    }

    /**
     * @return \AliSaleem\Curl\Response
     */
    public function request($method = 'GET')
    {
        $this->setMethod(\strtoupper($method));

        $this->curlHandle = \curl_init();
        \curl_setopt_array($this->curlHandle,
                           [
                               CURLOPT_URL            => $this->getUrlWithParameters(),
                               CURLOPT_CUSTOMREQUEST  => $this->getMethod(),
                               CURLOPT_RETURNTRANSFER => $this->getOptionOrDefault('return_body', true),
                               CURLOPT_FOLLOWLOCATION => $this->getOptionOrDefault('follow_redirects', true),
                               CURLOPT_HEADER         => $this->getOptionOrDefault('header', true),
                               CURLINFO_HEADER_OUT    => $this->getOptionOrDefault('return_header', true),
                               CURLOPT_HTTPHEADER     => $this->getCurlFormattedHeaders(),
                               CURLOPT_TIMEOUT        => $this->getOptionOrDefault('timeout', 30),
                               CURLOPT_SSL_VERIFYPEER => $this->getOptionOrDefault('verify_peer', false),
                           ]);
        if ($this->httpAuth) {
            \curl_setopt_array($this->curlHandle,
                               [
                                   CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                                   CURLOPT_USERPWD  => $this->httpAuth,
                               ]);
        }
        \curl_setopt_array($this->curlHandle, $this->options);
        switch ($this->getMethod()) {
            case 'DELETE':
            case 'OPTIONS':
            case 'HEAD':
            case 'GET':
                break;
            case 'POST':
            case 'PUT':
                \curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $this->getBodyAsString());
                break;
            default:
                throw new \UnexpectedValueException(\sprintf('Unknown HTTP method "%s"', $this->getMethod()));
        }
        $response = new $this->responseModel($this);
        return $response->execute();
    }

    public function addParameter($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }

    public function getParameter($name)
    {
        return (isset($this->parameters[$name])) ? $this->parameters[$name] : null;
    }

    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    public function getHeader($name)
    {
        return (isset($this->headers[$name])) ? $this->headers[$name] : null;
    }

    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    public function getOption($name)
    {
        return (isset($this->options[$name])) ? $this->options[$name] : null;
    }

    public function getMethod()
    {
        return $this->method;
    }

    protected function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrlWithParameters()
    {
        return $this->getUrl() . ($this->getParameters()
                ? ((\strpos($this->getUrl(), '?') === false)
                    ? '?'
                    : '&'
                  ) . \http_build_query($this->getParameters())
                : '');
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getBodyAsString()
    {
        return \is_string($this->getBody()) ? $this->getBody() : \json_encode($this->getBody());
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getCurlFormattedHeaders()
    {
        $headers = [];
        foreach ($this->getHeaders() as $name => $value) {
            $headers[] = $name . ":" . $value;
        }
        return $headers;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function getOptionOrDefault($name, $defaultValue)
    {
        return isset($this->options[$name]) ? $this->options[$name] : $defaultValue;
    }

    public function getCurlHandle()
    {
        return $this->curlHandle;
    }

    public function setValidateFunction($class, $function)
    {
        $this->validateFunction = [
            $class,
            $function,
        ];
        return $this;
    }

    public function getValidateFunction()
    {
        return $this->validateFunction;
    }

    public function setHttpAuth($username, $password)
    {
        $this->httpAuth = $username . ':' . $password;
        return $this;
    }

    public function setResponseModel($model)
    {
        $this->responseModel = $model;
    }

    public function getResponseModel()
    {
        return $this->responseModel ?: Response::class;
    }
}

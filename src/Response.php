<?php

namespace AliSaleem\Curl;

class Response
{
    /* @var $request \AliSaleem\Curl\Request */
    protected $request;
    protected $body;
    protected $headers;
    protected $httpCode;
    protected $info;
    protected $errorCode;
    protected $error;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function execute()
    {
        $attempt = 0;
        $tries   = $this->request->getOptionOrDefault('tries', 1);
        $valid   = false;
        while ($attempt < $tries && !$valid) {
            $attempt++;
            if ($curlResponse = \curl_exec($this->request->getCurlHandle())) {
                $this->info      = \curl_getinfo($this->request->getCurlHandle());
                $this->httpCode  = \intval($this->info['http_code']);
                $this->headers   = $this->parseResponseHeaders(\substr($curlResponse, 0, $this->info['header_size']));
                $this->body      = \substr($curlResponse, $this->info['header_size']);
                $this->errorCode = 0;
                $this->error     = null;
                $valid           = true;
            } else {
                $this->info      = [];
                $this->httpCode  = 444;
                $this->headers   = [];
                $this->body      = null;
                $this->errorCode = \curl_errno($this->request->getCurlHandle());
                $this->error     = \curl_error($this->request->getCurlHandle());
            }
            if ($this->request->getValidateFunction()) {
                $valid = \call_user_func_array($this->request->getValidateFunction(), [$this]);
            }
            if (!$valid && $attempt < $tries) {
                \usleep($this->request->getOptionOrDefault('delay', 0) * 1000);
            }
        }
        \curl_close($this->request->getCurlHandle());
        return $this;
    }

    public function isValid()
    {
        $valid = $this->getErrorCode() === 0;
        if ($this->request->getValidateFunction()) {
            $valid = \call_user_func_array($this->request->getValidateFunction(), [$this]);
        }
        return $valid;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getError()
    {
        return $this->error;
    }

    protected function parseResponseHeaders($curlHeaders)
    {
        $headers = [];
        $key     = '';
        foreach (\explode("\n", $curlHeaders) as $h) {
            $h = \explode(':', $h, 2);
            if (isset($h[1])) {
                if (!isset($headers[$h[0]]) || !$this->request->getOptionOrDefault('multiple_headers', false)) {
                    $headers[\trim($h[0])] = \trim($h[1]);
                } elseif (\is_array($headers[$h[0]])) {
                    $headers[\trim($h[0])] = \array_merge($headers[$h[0]], [\trim($h[1])]);
                } else {
                    $headers[\trim($h[0])] = \array_merge([$headers[$h[0]]], [\trim($h[1])]);
                }
                $key = $h[0];
            } else {
                if (\substr($h[0], 0, 1) == "\t") {
                    $headers[$key] .= "\r\n\t" . \trim($h[0]);
                } elseif (!$key && \trim($h[0])) {
                    $headers[0] = \trim($h[0]);
                }
            }
        }
        return $headers;
    }

    public function getBodyAsObject()
    {
        return \json_decode($this->getBody());
    }
}
